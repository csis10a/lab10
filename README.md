## Getting Started

1. Open a terminal, command prompt, or PowerShell and change to your working directory (e.g. U:)
2. Grab a copy of lab09 using: `git clone https://bitbucket.org/csis10a/lab10.git`
3. Follow the instructions at [https://docs.google.com/document/d/1-ElT3K8f929NSh9Wy2kBm4OYiEEQ4NtgH9FEHLgu_ls/edit?usp=sharing](https://docs.google.com/document/d/1-ElT3K8f929NSh9Wy2kBm4OYiEEQ4NtgH9FEHLgu_ls/edit?usp=sharing)

__Note:__ If git is not installed on your computer you can download the lab from [https://bitbucket.org/csis10a/lab10/get/master.zip](https://bitbucket.org/csis10a/lab10/get/master.zip)
